<?php

namespace App\Tests\Controller\Recipe;

use App\Controller\Recipe\RecipeSearchController;
use App\Controller\Recipe\RecipeShowController;
use PHPUnit\Framework\TestCase;

class RecipeControllerTest extends TestCase
{

    public function testSearch()
    {
        $recipe = new RecipeSearchController();

        $r = $recipe->search( 'curry' );

        if( $dataJson = json_decode( $r->getContent()  ) ){

            $this->assertEquals( 1, 1 );

        }else $this->assertEquals( 1, 0 );
    }

    public function testShow()
    {
        $testId = 10;
        $recipe = new RecipeShowController();

        $r = $recipe->show( $testId );

        if( $dataJson = json_decode( $r->getContent()  ) ){

            $this->assertEquals( $testId, $dataJson[0]->id );

        }else $this->assertEquals( 1, 0 );
    }
}