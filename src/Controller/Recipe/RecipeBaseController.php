<?php

namespace App\Controller\Recipe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RecipeBaseController extends AbstractController
{
    protected $apiBase = 'https://api.punkapi.com';

    protected function getNotFoundResponse(){

        $notFound = new Response();
        $notFound->headers->set( 'Content-Type', 'application/json; charset=utf-8' );
        $notFound->setStatusCode( Response::HTTP_NOT_FOUND );

        return $notFound;
    }

    /**
     * @Route("/", name="recipe")
     */
    public function index( Request $rq ): Response{
        
        return $this->render( 'recipe/page/home.html.twig' );
    }
}