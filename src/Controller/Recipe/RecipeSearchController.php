<?php

namespace App\Controller\Recipe;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RecipeSearchController extends RecipeBaseController
{
    /**
     * @Route("recipe/search/{q}", name="recipe_search")
     */
    public function search( $q ): Response{

        $res = $this->getNotFoundResponse();

        if( $q ){

            $client = new \GuzzleHttp\Client([
                'base_uri' => $this->apiBase,
                'timeout'  => 3
            ]);

            $response = $client->get( 
                'v2/beers',[ 'query' => 'food=' . $q ]
            );

            if( $response->getStatusCode() == 200 ){

                $body = $response->getBody();

                if( $body != '[]' ){

                    $dataJson = json_decode( $body );
                    $dataJsonMap = array_map( function( $dataItem ) {
            
                        $newData = [ 
                            'id' => $dataItem->id,
                            'name' => $dataItem->name,
                            'description' => $dataItem->description
                        ];
            
                        return $newData;
                    }, $dataJson );

                    $res = new JsonResponse( $dataJsonMap );
                }
            }
        }

        return $res;
    }
}