<?php

namespace App\Controller\Recipe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RecipeShowController extends RecipeBaseController
{
    /**
     * @Route("recipe/show/{id}", name="recipe_show")
     */
    public function show( $id ): Response{

        $res = $this->getNotFoundResponse();

        if( $id ){

            $client = new \GuzzleHttp\Client([
                'base_uri' => $this->apiBase,
                'timeout'  => 3
            ]);

            $response = $client->get( 
                'v2/beers',[ 'query' => 'ids=' . $id ]
            );

            if( $response->getStatusCode() == 200 ){

                $body = $response->getBody();

                if( $body != '[]' ){

                    $dataJson = json_decode( $body );
                    $dataJsonMap = array_map( function( $dataItem ) {
            
                        $newData = [ 
                            'id' => $dataItem->id,
                            'name' => $dataItem->name,
                            'description' => $dataItem->description,
                            'imagen' => $dataItem->image_url,
                            'tagline' => $dataItem->tagline,
                            'first_brewed' => $dataItem->first_brewed
                        ];
            
                        return $newData;
                    }, $dataJson );

                    $res = new JsonResponse( $dataJsonMap );
                }
            }
        }
       
        return $res;
    }
}